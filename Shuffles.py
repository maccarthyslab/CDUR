#!/usr/bin/python3

from __future__ import division
import os
import numpy
import sys
import argparse
import re
from random import shuffle,random,randint,choice,seed
from collections import Counter,defaultdict
from os import system
from subprocess import call
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio import SeqIO,Entrez
from Bio.SeqFeature import SeqFeature,FeatureLocation
import ensembl_rest as erest



# utils = importr("utils")
# plyr = importr("plyr")
# seqinr = importr("seqinr")

class Shuffles:

    def __init__(self, seq):
        self.seq = seq.upper()
        self.nts = ['A','C','G','T']
        self.tt = {"TTT":"F|Phe","TTC":"F|Phe","TTA":"L|Leu",
                   "TTG":"L|Leu","TCT":"S|Ser","TCC":"S|Ser",
                   "TCA":"S|Ser","TCG":"S|Ser", "TAT":"Y|Tyr",
                   "TAC":"Y|Tyr","TAA":"*|Stp","TAG":"*|Stp",
                   "TGT":"C|Cys","TGC":"C|Cys","TGA":"*|Stp",
                   "TGG":"W|Trp", "CTT":"L|Leu","CTC":"L|Leu",
                   "CTA":"L|Leu","CTG":"L|Leu","CCT":"P|Pro",
                   "CCC":"P|Pro","CCA":"P|Pro","CCG":"P|Pro",
                   "CAT":"H|His","CAC":"H|His","CAA":"Q|Gln",
                   "CAG":"Q|Gln","CGT":"R|Arg","CGC":"R|Arg",
                   "CGA":"R|Arg","CGG":"R|Arg", "ATT":"I|Ile",
                   "ATC":"I|Ile","ATA":"I|Ile","ATG":"M|Met",
                   "ACT":"T|Thr","ACC":"T|Thr","ACA":"T|Thr",
                   "ACG":"T|Thr", "AAT":"N|Asn","AAC":"N|Asn",
                   "AAA":"K|Lys","AAG":"K|Lys","AGT":"S|Ser",
                   "AGC":"S|Ser","AGA":"R|Arg","AGG":"R|Arg",
                   "GTT":"V|Val","GTC":"V|Val","GTA":"V|Val",
                   "GTG":"V|Val","GCT":"A|Ala","GCC":"A|Ala",
                   "GCA":"A|Ala","GCG":"A|Ala", "GAT":"D|Asp",
                   "GAC":"D|Asp","GAA":"E|Glu","GAG":"E|Glu",
                   "GGT":"G|Gly","GGC":"G|Gly","GGA":"G|Gly","GGG":"G|Gly"}

    def __str__(self):
        return self.seq
    #Following methods or their combinations produce randomized or scrambled nucleotide sequence from input sequence.
    #Amino-acid sequence of derived sequence is identical to the input sequence, but nucleotide composition (GC-, nucleotide, or dinucleotide content) may differ slightly for randomized sequences.


    def gc3(self, **kwargs):#this function creates sequence with GC-content close to the GC-content of the input sequence, but counts of nucleotides may differ from input sequence.
        seq = kwargs.get('seq',self.seq)
        gc=at=0
        for num in range(2,len(seq),3):#first calculating A+T and G+C of the input sequence in third codon position
            if seq[num]=='A'or seq[num]=='T':
                at+=1
            elif seq[num]=='G'or seq[num]=='C':
                gc+=1
        at=at/(len(seq)/3.)
        gc=gc/(len(seq)/3.)
        
        seq1=[]
        for num in range(2,len(seq),3):#list "seq1" will contain the first two nt of codon, third codon position will contain flags for subsequent randomization. Flags ('_Y_','_R_','_H_',or '_N_') correspond to IUPAC single-letter code, Y-Pyrimindine(C or T), R-Purine(A or G), H-Not G(A or C or T), N-any.
            seq1.append(seq[num-2:num])
            if (seq[num]=='T'or seq[num]=='C')and(seq[num-2:num]=='TT'or seq[num-2:num]=='TA'or seq[num-2:num]=='TG'or seq[num-2:num]=='CA'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GA'):
                seq1.append('_Y_')
            elif (seq[num]=='A'or seq[num]=='G')and(seq[num-2:num]=='TT'or seq[num-2:num]=='CA'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GA'):
                seq1.append('_R_')
            elif seq[num-2:num+1]=='ATT'or seq[num-2:num+1]=='ATC'or seq[num-2:num+1]=='ATA':
                seq1.append('_H_')
            elif (seq[num]=='A'or seq[num]=='G'or seq[num]=='T'or seq[num]=='C')and(seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CG'or seq[num-2:num]=='AC'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GG'):
                seq1.append('_N_')
            else: seq1.append(seq[num])

        seq2=''#"seq2" will contain the derived sequence, approproate nucleotide is chosen for flags in "seq1", according to GC-content
        for i in seq1:
            if i == '_Y_':
                x=random()
                if x<=gc:
                    seq2+='C'
                elif gc<x<=gc+at:
                    seq2+='T'
                else: seq2+=choice('TC')
            elif i == '_R_':
                x=random()
                if x<=gc:
                    seq2+='G'
                elif gc<x<=gc+at:
                    seq2+='A'
                else: seq2+=choice('AG')
            elif i == '_H_':
                x=random()
                if x<=gc:
                    seq2+='C'
                elif gc<x<=gc+at:
                    seq2+=choice('AT')
                else: seq2+=choice('ATC')
            elif i == '_N_':
                x=random()
                if x<=gc:
                    seq2+=choice('GC')
                elif gc<x<=gc+at:
                    seq2+=choice('AT')
                else: seq2+=choice('AGTC')
            else: seq2+=i
        seq=seq2
        return seq   


    def n3(self, **kwargs):#this function creates scrambled sequence with the numbers of each nucleotide identical to the input sequence.
        seq = kwargs.get('seq',self.seq)
        Y=[]
        seq1=[]
        for num in range(2,len(seq),3):
            if (seq[num]=='T' or seq[num]=='C')and(seq[num-2:num]=='TT'or seq[num-2:num]=='TC'or seq[num-2:num]=='TA'or seq[num-2:num]=='TG'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CA'or seq[num-2:num]=='CG'or seq[num-2:num]=='AT'or seq[num-2:num]=='AC'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GU'or seq[num-2:num]=='GC'or seq[num-2:num]=='GA'or seq[num-2:num]=='GG'):
                Y.append(seq[num])
                seq1+=[seq[num-2:num],'_Y_']
            else:seq1.append(seq[num-2:num+1])
        #now "seq1" contains flag '_Y_' in the third position of all codons, where C->T or T->C shuffling preserves the aminoacid sequence (i.e. PHE, SER etc.).
        #C and T from the original sequence in this case would be extracted into list "Y"
        shuffle(Y)#shuffling of list "Y". For example, before shuffling "Y" is ['C','T','C']; after - ['T','C','C']or['C','C','T']or['C','T','C']
        seq2=''
        for i in range(len(seq1)):
            if seq1[i]=='_Y_': seq2+=Y.pop(0)#now elements of "Y" are inserted back into the sequence instead of '_Y_', but in a different order compared to the input sequence
            else: seq2+=seq1[i]
        seq=seq2

        R=[]#similar to the previous step, but A and G are shuffled
        seq1=[]
        for num in range(2,len(seq),3):
            if (seq[num]=='A' or seq[num]=='G')and(seq[num-2:num]=='TT'or seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CA'or seq[num-2:num]=='CG'or seq[num-2:num]=='AC'or seq[num-2:num]=='AA'or seq[num-2:num]=='AG'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GA'or seq[num-2:num]=='GG'):
                R.append(seq[num])
                seq1+=[seq[num-2:num],'_R_']
            else:seq1.append(seq[num-2:num+1])
        shuffle(R)
        seq2=''
        for i in range(len(seq1)):
            if seq1[i]=='_R_':seq2+=R.pop(0)
            else:seq2+=seq1[i]
        seq=seq2
        

        H=[]#similar to the previous step, but A,C, and T are shuffled. Affected aminoacids are ILE (three codons), four-codon and four-codon portion of six-codon aminoacids.
        seq1=[]
        for num in range(2,len(seq),3):
            if (seq[num]=='A'or seq[num]=='C'or seq[num]=='T')and(seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CG'or seq[num-2:num]=='AT'or seq[num-2:num]=='AC'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GG'):
                H.append(seq[num])
                seq1+=[seq[num-2:num],'_H_']
            else:seq1.append(seq[num-2:num+1])

        shuffle(H)
        seq2=''
        for i in range(len(seq1)):
            if seq1[i]=='_H_':seq2+=H.pop(0)
            else:seq2+=seq1[i]
        seq=seq2

        N=[]#Shuffling of all four nucleotides, where possible. Affected aminoacids are four-codons and four-codon portion of six-codon aminoacids.
        seq1=[]
        for num in range(2,len(seq),3):
            if (seq[num]=='A'or seq[num]=='C'or seq[num]=='T'or seq[num]=='G')and(seq[num-2:num]=='TC'or seq[num-2:num]=='CT'or seq[num-2:num]=='CC'or seq[num-2:num]=='CG'or seq[num-2:num]=='AC'or seq[num-2:num]=='GT'or seq[num-2:num]=='GC'or seq[num-2:num]=='GG'):
                N.append(seq[num])
                seq1+=[seq[num-2:num],'_N_']
            else:seq1.append(seq[num-2:num+1])
        shuffle(N)
        seq2=''
        for i in range(len(seq1)):
            if seq1[i]=='_N_':seq2+=N.pop(0)
            else:seq2+=seq1[i]
        seq=seq2        
        return seq
        
        
    def dn23(self, **kwargs):#this function creates a randomized sequence, with dinucleotide frequences in codon position 2-3 close to those of the input sequence (not exact since this only counts dinucleotide frequency in second and third positions).
        seq = kwargs.get('seq',self.seq)
        aa=ag=ac=at=ga=gg=gc=gt=ca=cg=cc=ct=ta=tg=tc=tt=0
        for num in range(2,len(seq),3):#first calculating dinucleotide frequences in codon position 2-3 
            if seq[num-1]=='A':
                if seq[num]=='A':
                    aa+=1
                elif seq[num]=='G':
                    ag+=1
                elif seq[num]=='C':
                    ac+=1
                elif seq[num]=='T':
                    at+=1
            elif seq[num-1]=='G':
                if seq[num]=='A':
                    ga+=1
                elif seq[num]=='G':
                    gg+=1
                elif seq[num]=='C':
                    gc+=1
                elif seq[num]=='T':
                    gt+=1
            elif seq[num-1]=='C':
                if seq[num]=='A':
                    ca+=1
                elif seq[num]=='G':
                    cg+=1
                elif seq[num]=='C':
                    cc+=1
                elif seq[num]=='T':
                    ct+=1
            elif seq[num-1]=='T':
                if seq[num]=='A':
                    ta+=1
                elif seq[num]=='G':
                    tg+=1
                elif seq[num]=='C':
                    tc+=1
                elif seq[num]=='T':
                    tt+=1
        aa,ag,ac,at,ga,gg,gc,gt,ca,cg,cc,ct,ta,tg,tc,tt=aa/(len(seq)/3.),ag/(len(seq)/3.),ac/(len(seq)/3.),at/(len(seq)/3.),ga/(len(seq)/3.),gg/(len(seq)/3.),gc/(len(seq)/3.),gt/(len(seq)/3.),ca/(len(seq)/3.),cg/(len(seq)/3.),cc/(len(seq)/3.),ct/(len(seq)/3.),ta/(len(seq)/3.),tg/(len(seq)/3.),tc/(len(seq)/3.),tt/(len(seq)/3.)
        seq2=''
        for num in range(2,len(seq),3):#now each codon is replaced with a synonymous codon according to the dinucleotide frequences in codon position 2-3 
            seq2+=seq[num-2:num]
            #print seq2
            if seq[num-1]=='A'and seq[num-2:num+1]!='TAA'and seq[num-2:num+1]!='TAG':
                if seq[num]=='T'or seq[num]=='C':
                    space=at+ac
                    AT,AC=at/space,ac/space
                    x=random()
                    if x<=AT:
                        seq2+='T'
                    elif AT<x<=AT+AC:
                        seq2+='C'
                elif seq[num]=='A'or seq[num]=='G':
                    space=aa+ag
                    AA,AG=aa/space,ag/space
                    x=random()
                    if x<=AA:
                        seq2+='A'
                    elif AA<x<=AA+AG:
                        seq2+='G'
                else:seq2+=seq[num]
            elif seq[num-1]=='G'and seq[num-2:num+1]!='TGA'and seq[num-2:num+1]!='TGG':
                if (seq[num-2]=='T'or seq[num-2]=='A')and(seq[num]=='C'or seq[num]=='T'):
                    space = gt+gc
                    GT,GC=gt/space,gc/space
                    x=random()
                    if x<=GT:
                        seq2+='T'
                    elif GT<x<=GT+GC:
                        seq2+='C'
                elif seq[num-2:num+1]=='AGA'or seq[num-2:num+1]=='AGG':
                    space=ga+gg
                    GA,GG=ga/space,gg/space
                    x=random()
                    if x<=GA:
                        seq2+='A'
                    elif GA<x<=GA+GG:
                        seq2+='G'
                elif seq[num-2]=='C'or seq[num-2]=='G':
                    space=ga+gg+gc+gt
                    GA,GG,GC,GT=ga/space,gg/space,gc/space,gt/space
                    x=random()
                    if x<=GA:seq2+='A'
                    elif GA<x<=GA+GG:seq2+='G'
                    elif GA+GG<x<=GA+GG+GC:seq2+='C'
                    elif GA+GG+GC<x<=GA+GG+GC+GT:seq2+='T'
                else:seq2+=seq[num]
            elif seq[num-1]=='C':
                space=ca+cg+cc+ct
                CA,CG,CC,CT=ca/space,cg/space,cc/space,ct/space
                x=random()
                if x<=CA:seq2+='A'
                elif CA<x<=CA+CG:seq2+='G'
                elif CA+CG<x<=CA+CG+CC:seq2+='C'
                elif CA+CG+CC<x<=CA+CG+CC+CT:seq2+='T'
            elif seq[num-1]=='T':
                if seq[num-2:num+1]=='TTT'or seq[num-2:num+1]=='TTC':
                    space = tt+tc
                    TT,TC=tt/space,tc/space
                    x=random()
                    if x<=TT:
                        seq2+='T'
                    elif TT<x<=TT+TC:
                        seq2+='C'
                elif seq[num-2:num+1]=='TTA'or seq[num-2:num+1]=='TTG':
                    space = ta+tg
                    TA,TG=ta/space,tg/space
                    x=random()
                    if x<=TA:
                        seq2+='A'
                    elif TA<x<=TA+TG:
                        seq2+='G'
                elif seq[num-2:num+1]=='ATT'or seq[num-2:num+1]=='ATC'or seq[num-2:num+1]=='ATA':
                    space=tt+tc+ta
                    TT,TC,TA=tt/space,tc/space,ta/space
                    x=random()
                    if x<=TA:seq2+='A'
                    elif TA<x<=TA+TC:seq2+='C'
                    elif TA+TC<x<=TA+TC+TT:seq2+='T'
                elif seq[num-2]=='C'or seq[num-2]=='G':
                    space=ta+tg+tc+tt
                    TA,TG,TC,TT=ta/space,tg/space,tc/space,tt/space
                    x=random()
                    if x<=TA:seq2+='A'
                    elif TA<x<=TA+TG:seq2+='G'
                    elif TA+TG<x<=TA+TG+TC:seq2+='C'
                    elif TA+TG+TC<x<=TA+TG+TC+TT:seq2+='T'
                else:seq2+=seq[num]
            else:seq2+=seq[num]
        seq=seq2
        return seq    
        
    def shuffle_reg(self,**kwargs):
        '''
         Takes the input sequence, randomly shuffles it, and puts it back together. 
         '''
        seq = kwargs.get('seq', self.seq)
        list_seq = list(seq)
        shuffle(list_seq)
        return "".join(list_seq)
    
    def shuffle_re(self, conserved, conserved_move = True, **kwargs):
        """
        'Conserved' kwarg is for segments of the sequence that must be conserved and may not be shuffled internally (e.g. ebox). 
        Conserved subsequences must be written as substrings! Patterns here are non-overlapping. One can use pythonic regex expressions.
        conserved_move determines if the conserved sub sequences are allowed to move within the sequences or remain in the same place.
        For example, if "AB" is the conserved sequence in "XABXXXX", then conserved_move=False means that we can move "AB" around, e.g.
        "XABXXXX" -> "XXXXABX", whereas True means it must remain "XABXXXX"
        """        
        seq = kwargs.get('seq',self.seq)
        conserved_start_stop = []
        for motifs in conserved:
            re_motif = re.compile(motifs) #re object of conserved feature
            matches = re_motif.finditer(seq)
            for match in matches:
                conserved_start_stop.append((match.start(),match.end()))
            
        
        final_seq = self.shuffle_pos(conserved_start_stop, conserved_move = conserved_move)
        return final_seq
        

    def shuffle_pos(self, conserved = [], conserved_move = True, **kwargs):
        """
        'Conserved' kwarg is like the shuffle_re method but uses starting and stopping positions of each motif as opposed to the regex.
        Conserved subsequences must be written as [(start1,stop1), ..., (startn,stopn)] with zero indexing.
        Only use non-overlapping patterns.
        """
        seq = kwargs.get('seq',self.seq)
        cut_seq = seq
        conserved_seqs = []

        for begin,end in conserved:
            motif = seq[begin:end]
            conserved_seqs.append(motif)
            cut_seq = cut_seq.replace(motif,"")
            
        seq2shuffle = cut_seq 
        list_seq = list(seq2shuffle)
        shuffle(list_seq)

        if conserved_move:
            for start,stop in conserved:
                motif = list(conserved_seqs.pop(0))
                for i in range(start,stop):
                    list_seq.insert(i, motif.pop(0))
            final_seq = "".join(list_seq)
            return final_seq
        else:
            for motif in conserved_seqs:
                insert_place = randint(0, len(list_seq))
                list_seq.insert(insert_place,motif)
            final_seq = "".join(list_seq)
            return final_seq

    def transcript_shuffle(self, seq_dict, shuffle = "n3", shuffle_number = 1, polyprotein = False, **kwargs):
        """
        This shuffle is designed for raw rna transcripts that have not yet gone through post-transcriptional modifications. 
        
        These are sequences that have 5'/3' UTRs, introns, and exons; polyproteins are treated as one long, unbroken CDS,
        as we assume that deamination would occur during transcription. 
        
        We concatenate the exons (or the CDS portions) and run one of the n3, gc3, and dn23 on that concatenation.
        
        We run a vanilla shuffler for the introns, UTRs, and other non-coding RNA separately.
        
        In practice, there will be code in place to accept a user input file detailing the user's sequence and determining
        which subsequences are coding and noncoding.

        The arguments here are the shuffle to use (gc3, n3, dn23), and lists of untranslated regions and CDS. 
        Default shuffle is n3.
        example dict = {'upstream':[(0,12)], "noncoding":[(18,31),(39,56)], "coding":[(12,18), (31,38), (56,62)], "downstream":[(62,69)]}
        """

        seq = kwargs.get('seq',self.seq)
        print(seq)
        final_seq_list = list(seq) #Once shuffles occur, we replace the elements in these places with their shuffled counterparts
        noncoding_seqs = defaultdict(str) #seqs to run vanilla shuffle, demarcated according to type
        coding_seq = "" #seq to run one of gc3, n3, dn23

        for coding_types in seq_dict:
            if coding_types.lower() == "coding" or coding_types.lower() == "exon": #coding
                #first shuffle coding portions....
                for start,stop in seq_dict[coding_types]:
                    coding_seq += seq[start:stop]
                
                #print(Seq(coding_seq).translate()) - check to see if the CDS portion is consistent
                
                if shuffle == "n3":
                    shuffled_coding_seq = self.n3(seq = coding_seq)
                elif shuffle == "gc3":
                    shuffled_coding_seq = self.gc3(seq = coding_seq)
                elif shuffle == "dn23":
                    shuffled_coding_seq = self.dn23(seq = coding_seq)
                

                #print(Seq(shuffled_coding_seq).translate()) - CDS portion seems consistent
                
                #then replace...
                ending = 0    
                for start,stop in seq_dict[coding_types]:
                    final_seq_list[start:stop] = shuffled_coding_seq[ending:ending+stop-start]
                    ending += stop-start

            else: #noncoding
                #first shuffle noncoding portions...
                for start,stop in seq_dict[coding_types]:
                    noncoding_seqs[coding_types] += seq[start:stop]
                noncoding_seqs[coding_types] = self.shuffle_reg(seq = noncoding_seqs[coding_types])

                #then replace...
                ending = 0
                for start,stop in seq_dict[coding_types]:
                    final_seq_list[start:stop] = noncoding_seqs[coding_types][ending:ending+stop-start]
                    ending += stop-start
        return final_seq_list

        

if __name__ == "__main__":

    #test this code here

    # seq = Shuffles('ATGTCCTCTTCCTCTTCCTCTTAG')

    # seq1 = Seq(str(seq),alphabet = IUPAC.unambiguous_dna)
    # gc3_seq = Seq(seq.gc3(),alphabet = IUPAC.unambiguous_dna)
    # n3_seq = Seq(seq.third_simple(),alphabet = IUPAC.unambiguous_dna)
    # dn23_seq = Seq(seq.dn23(),alphabet = IUPAC.unambiguous_dna)
    # #shuffled_seq = Seq(shuffle_seq(seq), alphabet = IUPAC.unambiguous_dna)

    # shuffles = [seq1,gc3_seq,n3_seq,dn23_seq]#,shuffled_seq]
    # print(str(seq))
    # for i in shuffles:
    #     print(i)
    for i in range(4):
        try:
            seq = Shuffles('AAATCTTCCTCAAAGTAGCCCTCCACCGAGTGCGAGGAACTACCACGTAGAGAAAAA')
            seq_dict = {'upstream':[(0,3)], "noncoding":[(12,19),(28,38)], "coding":[(3,12), (19,28), (38,50)], "downstream":[(50,57)]}
            #shuffled = seq.shuffle_re(["TT[GC]"], conserved_move = True)
            final = seq.transcript_shuffle(seq_dict)
            print("".join(final))
        except Exception:
            print("error")
