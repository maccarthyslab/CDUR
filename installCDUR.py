import subprocess
import os

subprocess.call(['pip', 'install', 'numpy']) # Install Numpy
subprocess.call(['pip', 'install', 'scipy']) # Install Scipy
subprocess.call(['pip', 'install', 'pandas']) # Install Pandas
subprocess.call(['pip', 'install', 'statsmodels']) # Install Statsmodels
subprocess.call(['pip', 'install', 'patsy']) # Install Patsy
subprocess.call(['pip', 'install', 'biopython']) # Install Biopython
#subprocess.call(['easy_install', '-f', 'http://biopython.org/DIST/', 'biopython'])
subprocess.call(['pip', 'install', 'cython']) # Install Cython
subprocess.call(['pip', 'install', 'pyfasta']) # Install Pyfasta
subprocess.call(['make']) #compile CDUR reporter (titled shmsim)

print("Package installation finished. Ready to run CDUR.")

